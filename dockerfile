FROM node:12.18.2 AS build-app 

WORKDIR /app

COPY package.json .

RUN yarn

COPY . .

RUN yarn build

FROM node:12.18.2

WORKDIR /app

COPY package.json .

RUN yarn --prod

COPY --from=build-app /app/.next /app/.next

COPY --from=build-app /app/public /app/public

COPY --from=build-app /app/next.config.js /app/next.config.js

EXPOSE 3000

ENTRYPOINT [ "yarn", "start" ]