import Paragraph from '@editorjs/paragraph'
import List from '@editorjs/list'
// import LinkTool from '@editorjs/link'
import Image from '@editorjs/image'
import Header from '@editorjs/header'
import Quote from '@editorjs/quote'
import Marker from '@editorjs/marker'
import Delimiter from '@editorjs/delimiter'
import Axios from 'axios'
import FormData from 'form-data'

export const EDITOR_JS_TOOLS = {
  paragraph: Paragraph,
  list: List,
  // linkTool: LinkTool,
  image: {
    class: Image,
    config: {
      uploader: {
        async uploadByFile(file: File) {
          const body = new FormData()
          body.append('file', file)
          console.log('uploading')
          const { data } = await Axios.post('https://coffee-blog.markkiti.me/images', body)
          return {
            success: 1,
            file: {
              url: data.url
            }
          }
        }
      },
      async uploadByUrl(url){
        return {
          success: 1,
          file: {
            url,
          }
        }
      }
    },
  },
  header: {
    class: Header,
    config: {
      levels: [1, 2, 3],
    }
  },
  // quote: Quote,
  // marker: Marker,
  delimiter: Delimiter,
}