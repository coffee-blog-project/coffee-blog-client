import React, {FC} from 'react'
import NextLink from 'next/link'
export interface CatagoryProps{
  image: string
  name: string
  href: string
}

const Catagory: FC<CatagoryProps> = ({ image,name,href }) =>{
  return (
    <NextLink href={href}>
      <div 
      className="p-2 my-1 cursor-pointer"
      style={{ 
        backgroundImage: `url("${image}")`
      }}>
        <p className="text-white text-center">{name}</p>
      </div>
    </NextLink>
  )
}

const Catagories = () => {
  return (
    <div className="mb-4">
      <p className="font-semibold text-2xl mb-4">Categories</p>
      <Catagory image="/img1@2x.png" name="PROCESS" href="/categories/process"/>
      <Catagory image="/img2@2x.png" name="HEALTH" href="/categories/health"/>
      <Catagory image="/img3@2x.png" name="SUSTAINABILITY" href="/categories/sustainability"/>
      <Catagory image="/img4@2x.png" name="INDUSTRY" href="/categories/industry"/>
      <Catagory image="/experience.png" name="EXPERIENCE" href="/categories/experience"/>
      <Catagory image="/other.png" name="OTHER" href="/categories/other"/>
    </div>
  )
}

export default Catagories