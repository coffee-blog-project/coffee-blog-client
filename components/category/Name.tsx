import { FC } from 'react'
import { Story } from '../../components/Stories'
import { gql, useQuery } from '@apollo/client'

export interface NameProps {
  name: string
}

const CategoryName: FC<NameProps> = ({ name }) => {
  return (
    <>
      <div className="flex">
        <img className="w-32 h-full" src="/bean.png" />
        <p className="text-xl font-mono mt-16 -ml-14">{name}</p>
      </div>
      <hr className="mb-6"/>
    </>
  )
}

export default CategoryName