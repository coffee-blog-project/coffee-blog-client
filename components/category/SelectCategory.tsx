import { FC, useState } from 'react'
import NextLink from 'next/link'

export interface CategoryProps {
  name: string
  value: string
  onClick?: (value) => void
  selected?: boolean
}

const Button: FC<CategoryProps> = ({ name, selected, value, onClick }) => {
  return selected ? (
    <div
      className="flex border rounded-lg py-3 pl-4 pr-6 mb-2 border-brown bg-selected-brown border-4 cursor-pointer"
      onClick={() => onClick && onClick(value)}>
      <img className="w-6 mr-4" src="/selected.svg"/>
      <p>{name}</p>
    </div>
  ) : (
    <div 
      className="flex border rounded-lg py-3 pl-4 pr-6 mb-2 cursor-pointer"
      onClick={() => onClick && onClick(value)}>
      <img className="w-6 mr-4" src="/circle.svg"/>
      <p>{name}</p>
    </div>
  )
}

interface ConfirmButtonProps {
  disabled?: boolean
  category: string
}

const ConfirmButton:FC<ConfirmButtonProps> = ({ disabled, category }) => {
  return disabled ? (
      <button className="bg-gray-200 p-4 rounded-full w-full mt-8 text-gray-400">OK</button>
  ) : (
    <NextLink href={`/blogs/write?category=${category}`}>
      <button className="bg-black text-white p-4 rounded-full w-full mt-8">OK</button> 
    </NextLink>
  )
}

const Select = () => {
  const [category, setCategory] = useState('')
  return (
    <div className="flex justify-center">
      <div>
        <p className="text-3xl font-serif my-6 md:-ml-16">Select Categories</p>
        <div className="flex">
          <img className="w-96 h-full md:-ml-16 hidden md:block" src="/select.png" />
          <div className="md:ml-12 mt-1">
            <Button 
              name="Process" 
              value="process" 
              onClick={value => setCategory(value)}
              selected={category === 'process'}/>
            <Button 
              name="Health" 
              value="health"
              onClick={value => setCategory(value)}
              selected={category === 'health'}
              />
            <Button 
              name="Sustainability" 
              value="sustainability"
              onClick={value => setCategory(value)}
              selected={category == 'sustainability'}
              />
            <Button 
              name="Industry" 
              value="industry"
              onClick={value => setCategory(value)}
              selected={category === 'industry'}
              />
            <Button 
              name="Experience" 
              value="experience"
              onClick={value => setCategory(value)}
              selected={category == 'experience'}
              />
            <Button 
              name="Other" 
              value="other"
              onClick={value => setCategory(value)}
              selected={category == 'other'}
              /> 
              <ConfirmButton disabled={!category} category={category}/>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Select