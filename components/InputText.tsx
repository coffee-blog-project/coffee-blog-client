import { FC, InputHTMLAttributes } from 'react'

const InputText: FC<InputHTMLAttributes<HTMLInputElement>> = (props) => {
  return (
    <input 
      {...props}
      className="px-4 py-2 placeholder-gray-400 text-gray-700 relative bg-white bg-white rounded-full text-sm border border-gray-400 outline-none focus:outline-none focus:shadow-outline w-full pr-10 my-1.5 lg:w-96" 
    />
  )
}

export default InputText