import React from 'react'

const Layout = (props : any) => {
  return (
    <div className="container max-w-screen-lg px-4 mx-auto">
      {props.children}
    </div>
  )
}

export default Layout
