import { useState, useEffect, FC } from 'react'

export interface ReviewFromData {
  productName?: string
  description?: string
}

export interface FormProps {
  value?: ReviewFromData
  onChange?: (data: ReviewFromData) => void
}

const Form:FC<FormProps> = ({ value, onChange }) => {
  const [title, setTitle] = useState(value?.productName)
  const [description, setDescription] = useState(value?.description)

  useEffect(() => {
    if (onChange) {
      onChange({
        productName: title,
        description,
      })
    }
  }, [title, description])

  useEffect(() => {
    const { productName, description } = value || {}
    setTitle(productName)
    setDescription(description)
  }, [value])

  return (
    <div className="mb-8 mt-16">
      <input 
        className="w-full text-3xl mb-4 p-2" 
        placeholder="| name" 
        value={title} 
        onChange={e => setTitle(e.currentTarget.value)} />
      <textarea 
        className="w-full h-20 p-2" 
        placeholder="| description..." 
        value={description} 
        onChange={e => setDescription(e.currentTarget.value)} />
    </div>
  )
}

export default Form