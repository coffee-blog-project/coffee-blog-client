const ReviewLayout = (props: any) => {
  return (
    <div className="container mx-auto max-w-180">
      {props.children}
    </div>
  )
}

export default ReviewLayout