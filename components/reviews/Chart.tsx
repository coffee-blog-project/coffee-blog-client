import { Radar } from 'react-chartjs-2'
import { FC } from 'react'

const labels = ['sweet', 'bitter', 'source', 'roasted', 'fragrance']

const options = {
  scale: {
    ticks: {
        max: 5,
        min: 0,
        stepSize: 1
    },
    pointLabels: {
      fontSize: 14
    },
  },
  legend: {
    display: false,
  },
}

export interface GraphProps {
  sweet: number
  bitter: number
  source: number
  roasted: number
  fragrance: number 
}

const Graph: FC<GraphProps> = (props) => {
  const graphData = {
    labels,
    datasets: [
      {
        label: '',
        order: labels.length,
        data: labels.map(label => props[label] || 0),
        backgroundColor: '#CCB59780',
        borderColor: '#765121',
        borderWidth: 1,
      },
    ],
  }
  return (
    <Radar data={graphData} options={options}/>
  )
}

export default Graph