import { FC, useState, useEffect } from 'react'

interface FlavorProps {
  flavor: string
  color: string
  selected?: boolean
  onClicked?: (value: boolean) => void
}

export const Flavor: FC<FlavorProps> = ({ flavor, color, selected, onClicked }) => {
  return (
    <>
      {selected ? (
        /**
         * Update clicked selected flavor
         */
        <div 
          className={`rounded-lg bg-${color} text-center py-4 text-white border-4 border-pink-500`}
          onClick={() => onClicked && onClicked(!selected)}>
            {flavor}
        </div>
      ) : (
        <div 
          className={`rounded-lg bg-${color} text-center py-4 text-white my-1`}
          onClick={() => onClicked && onClicked(!selected)}>
            {flavor}
        </div>
      )}
    </>
  ) 
}

export interface FlavorWheelValue {
  fruity?: boolean
  fermented?: boolean 
  green?: boolean
  other?: boolean
  roasted?: boolean
  spices?: boolean
  nutty?: boolean
  sweet?: boolean
  floral?: boolean
}

export interface FlavorWheelProps {
  value?: FlavorWheelValue
  onChange?: (value: FlavorWheelValue) => void
}

const FlavorWheel: FC<FlavorWheelProps> = ({ value, onChange }) => {
  const [flavors, setFlavors] = useState<FlavorWheelValue>({});

  useEffect(() => {
    onChange && onChange(flavors)
  }, [flavors])

  useEffect(() => {
    if (value) {
      setFlavors(value)
    }
  }, [value])

  return (
    <div>
      <p className="mb-4 mt-6">Coffee Flavor</p>
      <div className="grid grid-cols-2 md:grid-cols-4 gap-y-4 gap-x-4">
        <Flavor 
          flavor="Fruity" 
          color="fruity-flavor" 
          selected={flavors.fruity} 
          onClicked={value => setFlavors({
            ...flavors,
            fruity: value,
          })}/>
        <Flavor 
          flavor="Sour/Fermented"
          color="fermented-flavor"
          selected={flavors.fermented} 
          onClicked={value => setFlavors({
            ...flavors,
            fermented: value,
          })}/>
        <Flavor 
          flavor="Green/Vegetative" 
          color="green-flavor"
          selected={flavors.green} 
          onClicked={value => setFlavors({
            ...flavors,
            green: value,
          })}/>
        <Flavor 
          flavor="Other" 
          color="other-flavor"
          selected={flavors.other} 
          onClicked={value => setFlavors({
            ...flavors,
            other: value,
          })}/>
        <Flavor 
          flavor="Roasted" 
          color="roasted-flavor"
          selected={flavors.roasted} 
          onClicked={value => setFlavors({
            ...flavors,
            roasted: value,
          })}/>
        <Flavor 
          flavor="Spices" 
          color="spices-flavor"
          selected={flavors.spices} 
          onClicked={value => setFlavors({
            ...flavors,
            spices: value,
          })}/>
        <Flavor 
          flavor="Nutty/Cocoa" 
          color="nutty-flavor"
          selected={flavors.nutty} 
          onClicked={value => setFlavors({
            ...flavors,
            nutty: value,
          })}/>
        <Flavor 
          flavor="Sweet" 
          color="sweet-flavor"
          selected={flavors.sweet} 
          onClicked={value => setFlavors({
            ...flavors,
            sweet: value,
          })}/>
        <Flavor 
          flavor="Floral" 
          color="floral-flavor"
          selected={flavors.floral} 
          onClicked={value => setFlavors({
            ...flavors,
            floral: value,
          })}/>
      </div>
    </div>
  )
}

export default FlavorWheel