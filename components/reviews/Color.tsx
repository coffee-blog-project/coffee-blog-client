
import { FC } from 'react'

export interface ColorProps {
  color: string
  name: string
}

const Color: FC<ColorProps> = ({color, name})  => {
  return (
    <div className="m-1">
      <div className={`w-20 h-8 rounded-lg bg-${color}-flavor text-center text-white p-2 text-xs`}>{name}</div>
    </div>
  )
}

export interface FlavorProps {
  flavors: ColorProps
}

const Colors: FC<FlavorProps> = ({ flavors }) => {
  return (
    <div className="flex flex-wrap">
      {Object.keys(flavors)
        .filter(key => flavors[key] === true)
        .map(color => <Color color={color} name={color}/>)}
    </div>
  )
}

export default Colors
