
import Rating from 'react-rating'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCircle} from '@fortawesome/free-solid-svg-icons'
import { useState, useEffect, FC } from 'react'

export interface RateProps {
  onChange?: (data: number) => void
  title?: string
  value?: number
}

const Rate: FC<RateProps> = ({ title, onChange, value }) => {
  const [rate, setRate] = useState(0)

  useEffect(() => {
    if (onChange) {
      onChange(rate)
    }
  }, [rate])

  useEffect(() => {
    setRate(value)
  }, [value])

  return (
    <div className="mx-auto">
      <p>{title}</p>
      <Rating initialRating={rate} onChange={value => setRate(value)} emptySymbol={<FontAwesomeIcon className="text-gray-300 text-5xl md:m-2 m-1" icon={faCircle} />} fullSymbol={<FontAwesomeIcon className="text-light-brown text-5xl md:m-2 m-1" icon={faCircle} />}/>
    </div>
  )
}

export default Rate