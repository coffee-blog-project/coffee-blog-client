import React, {FC} from 'react'
import NextLink from 'next/link'
import { gql, useQuery } from '@apollo/client'
import { DateTime } from 'luxon'
export interface StoryProps {
  _id: string
  imageHeaderUrl?: string
  title: string
  writer: {
    name: string
  }
  _createdAt: string
}

export const Story: FC<StoryProps> = ({_id, title, writer, _createdAt, imageHeaderUrl}) => {
  return (
    <NextLink href={`/blogs/${_id}`}>
      <div className="lg:mb-0 mb-6 cursor-pointer">
        <div className="aspect-w-3 aspect-h-2">
          <img className="w-full h-full object-cover" src={imageHeaderUrl || '/story-image1.png'}/>
        </div>
        <p>{title}</p>
        <p className="text-gray-400 text-sm">
          <span className="mr-1">
            {writer.name},
          </span>
          <span>
            {DateTime.fromISO(_createdAt).toLocaleString(DateTime.DATE_FULL)}
          </span>
        </p>
      </div>
    </NextLink>
  )
}

const GET_BLOGS = gql`
  query {
    blogs {
      _id
      _createdAt
      title
      imageHeaderUrl
      writer {
        _id
        name
      }
    }
  }
`

const Stories = () => {
  const getBlogs = useQuery(GET_BLOGS)
  if (getBlogs.loading) 
    return <p>loading...</p>
  if (getBlogs.error)
    return <p>{getBlogs.error.message}</p>

  return (
    <div className="flex-1">
      <p className="font-semibold text-2xl mb-4">Recent Stories</p>
      <div className="lg:grid lg:grid-cols-2 lg:gap-x-4 lg:gap-y-4 block">
        {getBlogs.data.blogs.map(story => <Story key={story._id} {...story} />)}
      </div>
    </div>
  )
}
export default Stories