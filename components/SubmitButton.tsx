import { ButtonHTMLAttributes, FC } from 'react'

const SubmitButton: FC<ButtonHTMLAttributes<HTMLButtonElement>> = ({children, ...props}) => {
  return (
    <button {...props} type="submit" className="w-full rounded-full bg-black text-white text-center py-2 mb-6">
      {children}
     </button>
  )
}

export default SubmitButton