import React from 'react'

const Footer = () => {
  return (
    <div className="bg-black w-full border-t border-grey p-4 pin-b absolute bottom-0 h-36">
      <div className="container max-w-screen-lg px-4 mx-auto py-6">
      <p className="text-white font-header text-2xl">COFFEE</p>
      <p className="text-white font-header text-2xl">BLOG</p>
      </div>
    </div>
  )
}

export default Footer