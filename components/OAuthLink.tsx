import NextLink from 'next/link'
import getConfig from 'next/config'

export const OAuthLink = () => {
  const config = getConfig()
  return (
    <div className="flex">
      <NextLink href={{
        protocol: 'https',
        host: 'www.facebook.com',
        pathname: '/v10.0/dialog/oauth',
        query: {
          client_id: config.publicRuntimeConfig.FACEBOOK_CLIENT_ID,
          redirect_uri: new URL('/facebook/callback', config.publicRuntimeConfig.APP_URL).href,
        }
      }}>
        <img src="/facebook-brands@2x.png" className="w-10 h-10 mr-4 cursor-pointer"/>
      </NextLink>
      <NextLink href={{
        protocol: 'https',
        host: 'accounts.google.com',
        pathname: '/o/oauth2/v2/auth',
        query: {
          client_id: config.publicRuntimeConfig.GOOGLE_CLIENT_ID,
          redirect_uri: new URL('/google/callback', config.publicRuntimeConfig.APP_URL).href,
          response_type: 'code',
          scope: 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile',
        }
      }}>
        <img src="/google-brands@2x.png" className="w-10 h-10 cursor-pointer"/>
      </NextLink>
    </div>
  )
}