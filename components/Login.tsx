import { FC, useState, InputHTMLAttributes } from 'react'
import NextLink from 'next/link'
import { useRouter } from 'next/router'
import { useMutation, gql } from '@apollo/client'
import InputText from '../components/InputText'
import SubmitButton from '../components/SubmitButton'
import { OAuthLink } from './OAuthLink'

const LOGIN = gql`
  mutation Login($email: String!, $password: String!) { 
    login(email: $email, password: $password)
  }
`

const LoginForm = () => {
  const router = useRouter()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState(null)
  const [login] = useMutation(LOGIN)

  return (
    <div className="container mx-auto h-screen flex justify-center items-center">
    <div className="mx-4 lg:w-96">
      <p className="font-header text-3xl text-center mb-6">COFFEE BLOG</p>
        {error && <p>{error}</p>}
        <form onSubmit={async e => {
          e.preventDefault()
          try {
            const { data } = await login({ variables: { email, password } })
            localStorage.setItem('token', data.login)
            router.push('/')
          } catch(e) {
            setError(e.message)
          }
        }}>
          <div className="mb-4">
            <InputText 
              placeholder="email" 
              value={email}
              onChange={e => setEmail(e.currentTarget.value)}/>
            <InputText 
              placeholder="password" 
              type="password"
              value={password}
              onChange={e => setPassword(e.currentTarget.value)}/>
          </div>
          <SubmitButton>
            Log in
          </SubmitButton>
        </form>
      <div>
        <p className="text-sm mr-4 mb-10">
          Do not have account? 
          <NextLink href="/register">
            <a className="underline"> create your account</a>
          </NextLink>
        </p>
        <div className="m-auto">
          <p className="text-xs mb-2 text-light-brown">log in with:</p>
          <OAuthLink />
        </div>
      </div>
    </div>
    </div>
  )
  }
  export default LoginForm