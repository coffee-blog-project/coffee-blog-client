import React, { FC } from 'react'
import NextLink from 'next/link'
import { gql, useQuery } from '@apollo/client'
import { DateTime } from 'luxon'

const GET_TOPWEEKLY = gql`
  query {
    topWeekly {
      to {
        _id
        type
      }
      source {
      ... on Blog {
        _id
        _createdAt
        writer {
          name
        }
        title
      }
      ... on Review {
        _id
        _createdAt
        writer {
          name
        }
        productName
      }
    }
    }
  }
`

export interface RankProps {
  _id: string
  __typename: string
  number: string
  title: string
  writer: {
    name: string
  }
  _createdAt: string
}

const Rank: FC<RankProps> = ({ _id, number, title, writer, _createdAt, __typename }) => {
  return (
    <NextLink href={`/${__typename.toLowerCase()}s/${_id}`}>
      <div className="flex mb-2 cursor-pointer">
        <div className="w-10 flex-none">
          <p className="text-3xl text-primary">{number}</p>
        </div>
        <div>
          <p>{title}</p>
          <p className="text-gray-400 text-sm">
            <span className="mr-1">{writer.name},</span>
            <span>{DateTime.fromISO(_createdAt).toLocaleString(DateTime.DATE_FULL)}</span>
          </p>
        </div>
      </div>
    </NextLink>
  )
}
   

const ShowRank = () => {
  const {error, loading, data } = useQuery(GET_TOPWEEKLY)
  if (loading) return <p>Loading...</p>
  if (error) return <p>{error.message}</p>
 console.log(data) 
  return(
    <div>
      <p className="font-semibold text-2xl mb-4">Top Weekly</p>
      {data.topWeekly.map(({ source }, index) => source.__typename === 'Blog' ? (
        <Rank key={source._id} number={index + 1} {...source} />
      ) : (
        <Rank key={source._id} number={index + 1} {...source} title={source.productName}/>
      ))}
    </div>
  )
}

export default ShowRank