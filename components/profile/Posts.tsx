import { FC } from 'react'
import { DateTime } from 'luxon'
import NextLink from 'next/link'
import { controllers } from 'chart.js'

export interface Writer {
  name: string
}

export interface StoryProp {
  __typename: 'Blog'
  imageHeaderUrl: string
  title: string
  writer: Writer
  _createdAt: string
  published?: boolean
  _id: string
}

export interface ReviewProp {
  __typename: 'Review'
  productImgUrl: string
  productName: string
  writer: Writer
  _createdAt: string
  published?: boolean
  _id: string
}

export const PublishStatus = () => {
  return (
    <div className="bg-light-green text-green rounded rounded-full w-20 text-center text-xs p-0.5">
      Published
    </div>
  )
}
export const DraftStatus = () => {
  return (
    <div className="bg-gray-200 text-gray-400 rounded rounded-full w-16 text-center text-xs p-0.5">
      Draft
    </div>
  )
}

export interface PostProp {
  __typename: string
  _id: string
  title: string
  _createdAt: string
  published?: boolean
  image: string
  writer: Writer
}

const Post: FC<PostProp> = ({image, title, writer, _createdAt, published, _id, __typename}) => {
  console.log(published)
  const href =  `/${__typename.toLowerCase()}s${published === undefined || published ? '' : '/write'}/${_id}`
  return (
    <NextLink href={href}>
      <div className="flex mb-6 cursor-pointer">
        <div className="w-32">
          <div className="aspect-w-3 aspect-h-2">
            <img className="w-full h-full object-cover" src={image || "/story-image1.png"}/>
          </div>
        </div>
        <div className="ml-4">
          <p>{title}</p>
          <div className="flex my-2">
            { __typename === 'Blog' ? (
            <div className="bg-light-yellow text-dark-yellow rounded rounded-full w-20 text-center text-xs p-0.5 mr-2">{__typename}</div> ) : (
            <div className="bg-sky-blue text-blue rounded rounded-full w-20 text-center text-xs p-0.5 mr-2">{__typename}</div> )  
            }
            {published !== undefined && (published ? (
              <PublishStatus />
            ) : (
              <DraftStatus />
            ))}
          </div>
          <p className="text-gray-400 text-sm">
            <span className="mr-1">
              {writer.name},
            </span>
            <span>
            {DateTime.fromISO(_createdAt).toLocaleString(DateTime.DATE_FULL)}
            </span>
          </p>
        </div>
      </div>
    </NextLink>
  )
}

interface PostsProp {
  data: (StoryProp | ReviewProp)[]
}

const Posts: FC<PostsProp> = ({ data }) => {
  console.log(data)
  return (
    <div className="max-w-xl flex-1">
      {data.map(post => post.__typename === 'Blog' ? (
        <Post {...post} image={post.imageHeaderUrl} />
      ) : (
        <Post 
          {...post} 
          title={post.productName}
          image={post.productImgUrl}
        />
      ))}
    </div>
  )
}

export default Posts