import { FC } from 'react'
import NextLink from 'next/link'
import { useRouter } from 'next/router'
interface LinkProps {
  href: string
  children: string
}

const Link: FC<LinkProps> = ({ href, children }) => {
  const router = useRouter()
  const active = href === router.pathname
  return (
    <NextLink href={href}>
      <p className={`cursor-pointer ${active && 'text-black'}`} >{children}</p>
    </NextLink>
  )
}

const Menu = () => {
  return (
    <div className="flex my-8 text-gray-400 active:text-black">
      <div className="mr-8">
        <Link href="/my/save">Save</Link>
        <hr />
      </div>
      <div className="mr-8">
        <Link href="/my/stories">My Stories</Link>
        <hr />
      </div>
      <div className="mr-8">
        <Link href="/my/reviews">My Reviews</Link>
        <hr />
      </div>
    </div>
  )
}

export default Menu