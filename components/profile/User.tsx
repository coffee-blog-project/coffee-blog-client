import { FC, HTMLAttributes } from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEdit} from '@fortawesome/free-regular-svg-icons'
import {faCogs} from '@fortawesome/free-solid-svg-icons'
import {faUserCircle} from '@fortawesome/free-solid-svg-icons'
import {faSignOutAlt} from '@fortawesome/free-solid-svg-icons'
import NextLink from 'next/link'
import {IconProp} from '@fortawesome/fontawesome-svg-core'
import { gql, useQuery } from '@apollo/client'
export interface LinkProps extends HTMLAttributes<HTMLDivElement> {
  icon?: IconProp
  href: string
  title?: string
}

const Link: FC<LinkProps> = ({ icon, href, title, ...props }) => {
  return (
  <NextLink href={href}>
    <div {...props} className="flex text-center cursor-pointer">
      <FontAwesomeIcon icon={icon}/>
      <p className="ml-2 mb-2">{title}</p>
    </div>
  </NextLink> 
  )
}
export interface UserProps {
  name: string
  imageUrl: string
}

const GET_ME = gql`
  query {
    me {
      _id
      name
      imageUrl
    }
  }
`

const User: FC<UserProps> = ({ }) => {
  const getMe = useQuery(GET_ME) 
  const { imageUrl } = getMe.data?.me || {}
  if (getMe.loading)
    return <p>loading...</p>
  return (
    <div className="mx-auto text-center text-gray-400 mt-8 md:mt-0 block md:flex-none">
      { imageUrl ? (
        <img className="rounded-full lg:w-30 lg:h-30 md:w-24 md:h-24 mb-4 mx-auto w-24 h-24 object-cover" src={getMe.data.me.imageUrl}/>) : (
          <FontAwesomeIcon className="lg:text-8xl mb-4 mx-auto text-8xl text-gray-300" icon={faUserCircle}/>
      )
      }
      <div className="flex justify-center">
      <div>
      <p className="text-black mb-8">{getMe.data.me.name}</p>
      <Link href="/blogs/write" icon={faEdit} title="Write your story"/>
      <Link href="/reviews/write" icon={faEdit} title="Write your review"/>
      <Link href="/settings" icon={faCogs} title="Settings"/>
      <Link 
        href="/" 
        onClick={() => {
          localStorage.removeItem('token')
          getMe.client.resetStore()
        }} 
        icon={faSignOutAlt} 
        title="Log out"/>
      </div>
      </div>
    </div>
  )
}

export default User