import React, { ButtonHTMLAttributes, FC } from 'react'
import NextLink from 'next/link'
import {FontAwesomeIcon, FontAwesomeIconProps} from '@fortawesome/react-fontawesome'
import {faSearch} from '@fortawesome/free-solid-svg-icons'
import {faUserCircle} from '@fortawesome/free-solid-svg-icons'
import { gql, useQuery } from '@apollo/client'

export const SaveDraftButton: FC<ButtonHTMLAttributes<HTMLButtonElement>> = (props) => {
  return <button {...props} className="disabled:hidden border rounded-full border-gray-400 sm:px-4 px-2 sm:text-sm md:text-base text-gray-400 text-xs">Save draft</button>
}

export const PostButton: FC<ButtonHTMLAttributes<HTMLButtonElement>> = (props) => {
  return <button {...props} className="border rounded-full border-black sm:px-4 px-3 sm:ml-2 sm:text-sm md:text-base text-xs ml-1">Post</button>
}

export const SearchButton: FC<Partial<FontAwesomeIconProps>> = (props) => {
  return (
    <NextLink href="/search">
      <FontAwesomeIcon {...props} className="mt-3 mr-2 cursor-pointer" icon={faSearch}/>
    </NextLink>
  )
}

export type HeaderElement = ReturnType<typeof SaveDraftButton | typeof PostButton | typeof SearchButton>

interface HeaderProps {
  children?: HeaderElement | HeaderElement[]
}

const GET_ME = gql`
  query {
    me {
      _id
      name
      imageUrl
    }
  }
`

const Header: FC<HeaderProps> = ({ children }) => {
  const getMe = useQuery(GET_ME)
  const { imageUrl } = getMe.data?.me || {}
  if (getMe.loading)
    return <p>loading...</p>
  return (
   <div className="relative">
    <div className="flex">
      <div className="md:mx-auto my-6">
        <NextLink href="/">
          <p className="font-header text-xl md:text-3xl text-center cursor-pointer">COFFEE BLOG</p>
        </NextLink>
      </div>
    </div>
      <div className="absolute right-0 top-6 flex">
        {children}
        {getMe.data ? (
          <NextLink href="/my/save">
            {imageUrl ? (
            <img className="w-10 h-10 rounded-full ml-2 cursor-pointer object-cover" src={getMe.data.me.imageUrl}/>) : (
             <FontAwesomeIcon className="text-4xl text-gray-300 cursor-pointer" icon={faUserCircle}/>
            )
            }
          </NextLink>
        ) : (
          <p className="mt-2 cursor-pointer ml-2">
            <NextLink href="/login">
              <span>Log in </span>
            </NextLink>
            /
            <NextLink href="/register">
              <span> Register</span>
            </NextLink>
          </p>
        )}
      </div>
     <hr />
   </div> 
  )
}

export default Header