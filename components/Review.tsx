import React, {FC} from 'react'
import NextLink from 'next/link'
import ScrollMenu from 'react-horizontal-scrolling-menu'
import { gql, useQuery } from '@apollo/client'
import { DateTime } from 'luxon'

export interface ReviewProps {
  _id: string
  productImgUrl?: string
  productName: string
  writer: {
    name: string
  }
  _createdAt: string
}

const Review: FC<ReviewProps> = ({_id, productImgUrl, productName, writer, _createdAt}) => {
  return (
    <NextLink href={`/reviews/${_id}`}>
      <div className="lg:mb-0 w-64 lg:w-56 md:w-96 mr-4 cursor-pointer">
        <div className="aspect-w-3 aspect-h-2">
          <img 
            className="w-full h-full object-cover"
            src={productImgUrl || '/story-image1.png'} draggable={false}
          />
        </div>
        <div className="">
          <h4>{productName}</h4> 
          <p className="text-gray-400 text-sm">
            <span className="mr-1">
              {writer.name}
            </span>
            <span>
              {DateTime.fromISO(_createdAt).toLocaleString(DateTime.DATE_FULL)}
            </span>
          </p> 
        </div>
      </div>
    </NextLink>
  )
}

const GET_REVIEW = gql`
  query {
    reviews {
      _id
      _createdAt
      productName
      productImgUrl
      content
      writer {
        _id
        name
      }
    }
  }
`

const Reviews = () => {
  const getReview = useQuery(GET_REVIEW)
  if (getReview.loading)
    return <p>loading...</p>

  if (getReview.error)
    return <p>{getReview.error.message}</p>

  return (
    <div className="mt-12 mb-16">
      <p className="font-semibold text-2xl mb-4">Reviews</p>
      <div className="lg:hidden">
        <ScrollMenu 
          data={getReview.data.reviews.map(review => <Review {...review} />)}
          wheel={false}
          alignCenter={false}
          menuStyle={{ touchAction: 'none' }}
          innerWrapperClass="flex"
        />
      </div>
      <div className="lg:grid lg:grid-cols-4 lg:gap-x-1 lg:gap-y-5 hidden">
        {getReview.data.reviews.map(review => <Review key={review._id} {...review} />)}
      </div>
    </div>
  )
}
export default Reviews