import { FC } from 'react'
import { DateTime } from 'luxon'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faUserCircle} from '@fortawesome/free-solid-svg-icons'

export const  HeaderComment = (props: any) => {
  return (
    <div className="mb-8">
      <hr />
      <p className="mt-8">Comments</p>
      {props.children}
    </div> 
  )
}

export interface CommentProps{
  content?: string
  name?: string 
  date?: string
  imageUrl?: string
}

const Comment: FC<CommentProps> = ({content, name, date, imageUrl}) => {
  return (
    <>
      <div className="flex mt-8">
        {imageUrl ? (
        <img className="rounded rounded-full w-14 h-14 object-cover" src={imageUrl} />) : (
         <FontAwesomeIcon className="text-5xl text-gray-300" icon={faUserCircle}/>
        )
        }
        <div className="ml-8 text-gray-500">
          <p>{name}</p>
          <p className="text-xs mb-2">{DateTime.fromISO(date).toLocaleString(DateTime.DATE_FULL)}</p>
        </div>
      </div>
      <div className="ml-24 mt-2 mb-6">
        <p>{content}</p>
      </div>
      <hr />
    </>
  )
}

export default Comment