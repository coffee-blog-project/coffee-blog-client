import { useState, FC } from 'react'
import { useMutation, useQuery, gql } from '@apollo/client'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faUserCircle} from '@fortawesome/free-solid-svg-icons'
import { useRouter } from 'next/router'

const COMMENT = gql`
  mutation CreateComment($comment: CreateCommentArgs!){
    createComment(
      comment: $comment
    ) {
      _id
      _createdAt
      from {
        user {
          name
        }
      }
      content
    }
}
`

const GET_BLOG = gql`
  query GetBlog($_id: String!) {
    blog(
      _id: $_id
    ) {
      _id
      _createdAt
      _updatedAt
      writerId
      title
      imageHeaderUrl
      contentFormat
      content
      writer {
        _id
        _createdAt
        _updatedAt
        name
      }
      comments {
        _createdAt
        from {
          user {
            name
            imageUrl
          }
        }
        content
      }
    }
  }
`

const GET_ME = gql`
  query {
    me {
      _id
      imageUrl
    }
  }
`

interface CreateCommentProps {
  blogId: string
}

const CreateComment: FC<CreateCommentProps> = ({ blogId }) => {
  const router = useRouter()
  const [content, setContent] = useState('')
  const { data, loading } = useQuery(GET_ME)
  const [createComment] = useMutation(COMMENT, {
    refetchQueries: [
      { query: GET_BLOG, variables: { _id: blogId } }
    ]
  })

  const { imageUrl } = data?.me || {}

  return (
    <div className="my-4">
      <div className="flex">
        {imageUrl ? (
        <img className="rouned rounded-full w-10 h-10 object-cover" src={imageUrl} />) : (
        <FontAwesomeIcon className="lg:text-8xl mb-4 mx-auto text-6xl text-gray-300" icon={faUserCircle}/> 
        )
        }
        <form
          className="w-full pl-4" 
          onSubmit={async e => {
            e.preventDefault()
            try {
            const { data } = await createComment({ 
              variables: { 
                comment: {
                  to: { type: 'blog', _id: blogId },
                  content,
                } 
              }
            })
          } catch(e) {
            router.push('/login')
          }
            setContent('')
          }}>
          <textarea className="border border-gray-200 w-full rounded-lg p-2"
            value={content}
            onChange = {e => setContent(e.currentTarget.value)}
          ></textarea>
          <div className="flex justify-end">
            <button type="submit" className="rounded-full py-2 px-4 text-sm mt-2 bg-light-brown text-white">Comment</button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default CreateComment