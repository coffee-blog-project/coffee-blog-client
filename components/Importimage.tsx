import { FC, useState, InputHTMLAttributes, useEffect } from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faImage} from '@fortawesome/free-solid-svg-icons'

export type ImportImageProps = InputHTMLAttributes<HTMLInputElement> & {
  imageUri?: string
}

const ImportImage: FC<ImportImageProps> = ({imageUri: initialImageUri, onChange, ...props}) => {
  const [imageUri, setImageUri] = useState<string>(null)

  useEffect(() => {
    console.log('imageuri', initialImageUri)
    if(initialImageUri) setImageUri(initialImageUri)
  }, [initialImageUri]);

  return (
    <div className="mt-8">
      <input 
        {...props} 
        type="file" 
        id="upload" 
        hidden 
        onChange={(e) => {
          setImageUri(URL.createObjectURL(e.target.files[0]))
          onChange && onChange(e)
        }}/>
      <label htmlFor="upload">
        <div className="relative w-full h-96">
          <div className="absolute flex h-full w-full m-auto justify-center items-center cursor-pointer">
            <FontAwesomeIcon className="text-6xl" icon={faImage}/>
          </div>
          <div className="h-full w-full bg-gray-200 text-center flex items-center justify-center">
            {imageUri && <img className="object-cover w-full h-full" src={imageUri} alt=""/>}
          </div>
        </div>
      </label>
    </div>
  )
}

export default ImportImage