import { MouseEvent} from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faHeart} from '@fortawesome/free-regular-svg-icons'
import {faHeart as faSolidHeart} from '@fortawesome/free-solid-svg-icons'
import { useMutation, gql, PureQueryOptions } from '@apollo/client'
import { FC } from 'react'
import { useRouter } from 'next/router'

const GET_BLOG = gql`
  query GetBlog($_id: String!) {
    blog(_id: $_id) {
      liked
      likes
      saved
    }
  }
`

const GET_REVIEW = gql`
  query GetReview($_id: String!) {
    review(_id: $_id) {
      liked
      likes
      saved
    }
  }
`

const CREATE_LIKE = gql`
  mutation CreateLike($like: CreateLikeArgs!){
    createLike(
      like: $like
    ) {
      _id
    }
  }
`

const DELETE_LIKE = gql`
  mutation DeleteLike($like: DeleteLikeArgs!){
    deleteLike(
      like: $like
    ) 
  }
`

const CREATE_SAVE = gql`
  mutation CreateSave($save: CreateSaveArgs!){
    createSave(
      save: $save
    ) {
      _id
    }
}
`
const DELETE_SAVE = gql`
  mutation DeleteSave($save: DeleteSaveArgs!){
    deleteSave(
      save: $save
    )
}
`

interface PanelProps {
  __typename: string
  _id: string
  liked: boolean
  saved: boolean
}


const Panel: FC<PanelProps> = ({ __typename, _id, liked, saved }) => {
  const router = useRouter()
  const refetchQueries: PureQueryOptions[] = [
    { query: __typename === 'Blog' ? GET_BLOG : GET_REVIEW, variables: { _id } }
  ]
  const [createLike] = useMutation(CREATE_LIKE, { refetchQueries, })
  const [deleteLike] = useMutation(DELETE_LIKE, { refetchQueries, }) 
  const [createSave] = useMutation(CREATE_SAVE, { refetchQueries, })
  const [deleteSave] = useMutation(DELETE_SAVE, { refetchQueries, })

  const onLikeClick = async (e: MouseEvent<HTMLDivElement, globalThis.MouseEvent>) => {
    e.preventDefault()
    const variables = {
      like: {
        to: {
          type: __typename.toLowerCase(), 
          _id,
        }
      }
    }
    console.log(variables)
    if (!liked) {
      try {
      const {data} = await createLike({
        variables,
        })
      console.log('liked')
      } catch(e) {
        router.push('/login') 
      }
    } else {
      await deleteLike({
        variables,
      })
      console.log('unliked')
    }
  }

  const onSaveClick = async (e: MouseEvent<HTMLDivElement, globalThis.MouseEvent>) => {
    e.preventDefault()
    const variables = {
      save: {
        to: {
          type: __typename.toLowerCase(),
          _id,
        }
      }
    }
    if (!saved) {
      console.log('saved')
      try {
        const {data} = await createSave({
          variables,
        })
      } catch(e) {
        router.push('/login')
      }
    } else {
      console.log('unsaved')
      await deleteSave({
        variables,
      })
    }
  }

  return (
    <>
      <div className="hidden md:block mt-4">
        <div className="flex">
          <div className="cursor-pointer" onClick={onLikeClick}>
            {liked ? (
              <FontAwesomeIcon className="text-xl mr-3" icon={faSolidHeart}/>
            ) : (
              <FontAwesomeIcon className="text-xl mr-3" icon={faHeart}/>
            )}
          </div>
          <p>|</p>
          <div className="cursor-pointer" onClick={onSaveClick}>
            {saved ? (
              <p className="ml-3">Saved</p>
            ) : (
              <p className="ml-3">Save</p>
             )
            }
          </div>
        </div>
      </div>
      <div className="bottom-0 left-0 fixed md:hidden bg-white w-full h-12 py-3 z-10 border-t-1 border">
        <div className="flex text-center">
          <div className="cursor-pointer flex-1" onClick={onLikeClick}>
            {liked ? (
              <FontAwesomeIcon className="mt-1" icon={faSolidHeart} />
            ) : (
              <FontAwesomeIcon className="mt-1" icon={faHeart} />
            )}
          </div>
          <p>|</p>
          <div className="cursor-pointer flex-1" onClick={onSaveClick}>
            {saved ? (
              <p className="ml-3 flex-1 text-center">Saved</p>
            ) : (
              <p className="ml-3 flex-1 text-center">Save</p> 
            )
            }
          </div>
        </div>
      </div>
    </>
  )
}

export default Panel