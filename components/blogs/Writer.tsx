import { FC } from 'react'
import { useRouter } from 'next/router'
import Panel from './Panel'
import {faUserCircle} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
export interface WriterProps {
  __typename: string
  _id: string
  author: string
  views: number
  likes: number
  liked: boolean
  saved: boolean
  imageUrl: string
}

const Writer: FC<WriterProps> = ({__typename, _id, author, views, likes, liked, saved, imageUrl}) => {
  return (
    <div>
      <div className="flex-none text-gray-500 hidden md:block m-auto lg:w-28 ">
        {imageUrl ? (
        <img className="rounded-full lg:w-24 lg:h-24 md:w-20 md:h-20 mb-4 object-cover" src={imageUrl}/>) : (
        <FontAwesomeIcon className="lg:text-8xl md:text-7xl mb-4 text-gray-300" icon={faUserCircle}/>
        )
        }
        <p>{author}</p>
        <p className="text-xs mb-2">Writer</p>
        <div className="text-sm">
          <p>{views} view{views > 1 && 's'}</p>
          <p>{likes} like{likes > 1 && 's'}</p>
          <Panel __typename={__typename} _id={_id} liked={liked} saved={saved}/>
        </div>
      </div>
      <div className="flex text-gray-500 md:hidden m-0 mt-2">
        <div className="mr-4">
          <p>{author}</p>
          <p className="text-xs mb-2">Writer</p>
        </div>
        <div className="text-sm flex mt-2 ml-4">
          <p className="mr-4">{views} view{views > 1 && 's'}</p>
          <p>{likes} like{likes > 1 && 's'}</p>
          <Panel __typename={__typename} _id={_id} liked={liked} saved={saved}/>
        </div>
      </div>
    </div>
  )
}

export default Writer