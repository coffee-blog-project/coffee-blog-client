const LayoutBlog = (props : any) => {
  return (
    <div className="container lg:pr-20 mx-auto lg:pl-12 md:pl-10">
      {props.children}
    </div>
  )
}

export default LayoutBlog