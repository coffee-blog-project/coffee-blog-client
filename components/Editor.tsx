import { FC, useState } from 'react'
import EditorJs from "react-editor-js";
import { OutputData, API } from '@editorjs/editorjs';
import { EDITOR_JS_TOOLS } from './tools';

interface EditorProps {
  data?: OutputData
  onChange?: (data: OutputData) => void
}

const INITIAL_DATA: OutputData = {
  blocks: [
    {
      type: 'header',
      data: {
        text: '',
        level: 1,
      }
    }
  ]
}

const Editor: FC<EditorProps> = ({ data = INITIAL_DATA, onChange }) => { 
  const [title, setTitle] = useState<string>(data?.blocks[0].data.text)
  const [hasInitialTitle, setInitial] = useState<boolean>(false)
  
  const onDataChange = (api: API, data: OutputData) => {
    console.log(data)
    const titleBlock = data.blocks[0]
    /**
     * Missing title create with backup title text
     */
    console.log(titleBlock)
    if (!hasInitialTitle && !(titleBlock?.type == 'header' && titleBlock?.data.level == 1)) {
      if (title.length === 0) {
        setInitial(true)
        api.blocks.insert('header', { text: 'Please insert title', level: 1, }, {}, 0, false)
        return
      }
    }
    /**
     * Update title state
     */
    setInitial(false)
    setTitle(titleBlock?.type === 'header' ? titleBlock?.data.text : '') 

    console.log(api)
    console.log(data)
    console.log(JSON.stringify(data))

    onChange && onChange(data)
  }

  return (
    <EditorJs
      placeholder="| Write your story..."
      tools={EDITOR_JS_TOOLS}
      onChange={onDataChange}
      data={data}
      autofocus
    />
  );
};

export default Editor;
