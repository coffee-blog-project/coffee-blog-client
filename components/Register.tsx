import React, { FC, useState } from 'react'
import NextLink from 'next/link'
import { useRouter } from 'next/router'
import InputText from '../components/InputText'
import { useMutation, gql } from '@apollo/client'
import SubmitButton from '../components/SubmitButton'
import { OAuthLink } from './OAuthLink'

const REGISTER = gql`
  mutation RegiserUser($user: CreateUserArgs!) {
  createUser(
    user: $user
  ) {
    _id
  }
}
`

const Register = () => {
  const router = useRouter()
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [error, setError] = useState(null)
  const [register] = useMutation(REGISTER)

  return (
    <div className="container mx-auto h-screen flex justify-center items-center">
    <div className="mx-4 lg:w-96">
      <p className="font-header text-3xl text-center mb-6">COFFEE BLOG</p>
      {error && <p>{error}</p>}
        <form onSubmit={async e => {
          e.preventDefault()
          try {
            if (password !== confirmPassword) throw new Error('Password not match.')
            const { data } = await register({ variables: { user: { name, email, password } } })
            console.log(data)
            router.push('/register/complete')
          } catch(e) {
            setError(e.message)
          }
        }}>
          <div className="mb-6">
            <InputText 
              placeholder="username"
              value={name}
              onChange={e => setName(e.currentTarget.value)}/>
            <InputText 
              placeholder="email"
              value={email}
              onChange={e => setEmail(e.currentTarget.value)} />
            <InputText 
              placeholder="password"
              type="password" 
              value={password}
              onChange={e => setPassword(e.currentTarget.value)} />
            <InputText 
              placeholder="confirm password"
              type="password"  
              value={confirmPassword}
              onChange={e => setConfirmPassword(e.currentTarget.value)} />
          </div>
          <SubmitButton>
            Register
          </SubmitButton>
        </form>
      <div className="mb-10">
        <p className="text-xs mb-4 text-light-brown">register with:</p>
        <OAuthLink />
      </div>
      <hr className="mb-4"/>
      <p className="text-center text-sm text-gray-500">Already have login? <NextLink href="/login"><a className="underline">log in</a></NextLink></p>
    </div>
    </div>
  )
}
export default Register