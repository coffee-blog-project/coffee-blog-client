import Resizer from 'react-image-file-resizer'

export const resizeImage = (uploadFile: File, maxWidth=640, maxHeight=640): Promise<File> => {
  return new Promise((resolve) => {
    Resizer.imageFileResizer(uploadFile, maxWidth, maxHeight, 'JPEG', 100, 0, (file: File) => {
      resolve(file)
    }, 'file')
  })
}
