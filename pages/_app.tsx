import { FC } from 'react'
import { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client'
import { client } from '../api/apollo'

import '../styles/globals.css'

const MyApp: FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <div className="min-h-screen relative">
    <div className="pb-36">
    <ApolloProvider client={client}>
      <Component {...pageProps} />
    </ApolloProvider>
    </div>
    </div>
  )
}

export default MyApp
