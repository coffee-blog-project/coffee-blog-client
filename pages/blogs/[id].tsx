import { FC } from 'react'
import { useRouter } from 'next/router'
import { gql, useQuery } from '@apollo/client'
import { OutputData } from '@editorjs/editorjs'
import Header from '../../components/Header'
import Layout from '../../components/Layout'
import LayoutBlog from '../../components/blogs/LayoutBlog'
import Writer from '../../components/blogs/Writer'
import Footer from '../../components/Footer'
import Comment, { HeaderComment } from '../../components/Comment'
import CreateComment from '../../components/CreateComment'

const GET_BLOG = gql`
  query GetBlog($_id: String!) {
    blog(
      _id: $_id
    ) {
      _id
      _createdAt
      _updatedAt
      writerId
      title
      imageHeaderUrl
      contentFormat
      content
      liked
      likes
      views
      saved
      writer {
        _id
        _createdAt
        _updatedAt
        name
        imageUrl
      }
      comments {
        _createdAt
        from {
          user {
            name
            imageUrl
          } 
        }
        content
      }
    }
  }
`

interface BlogHeaderProps {
  // type: 'header',
  data: {
    level: number
    text: string
  }
}

const BlogHeader: FC<BlogHeaderProps> = ({ data }) => {
  const { text, level } = data
  switch(level) {
    case 1:
      return <h1 className="font-header text-5xl mb-4">{text}</h1>
    case 2:
      return <h2 className="font-header text-3xl mb-4">{text}</h2>
    case 3:
      return <h3 className="font-header text-2xl mb-4">{text}</h3>
  }
}

interface BlogParagraphProps {
  // type: 'paragraph',
  data: {
    level: number
    text: string
  }
}

const BlogParagraph: FC<BlogParagraphProps> = ({ data }) => {
  return <p className="mb-4">{data.text}</p>
}

interface ImageParagraphProps {
  // type: 'image',
  data: {
    file: {
      url: string,
    }
    caption: string,
    withBorder: boolean,
    withBackground: boolean,
  }
}

const ImageParagraph: FC<ImageParagraphProps> = ({ data }) => {
  const { url } = data.file
  console.log(data.file)
  return (
    <>
      <img className="m-auto" src={url} />
      <p className="text-center text-gray-400">
        {data.caption}
      </p>
    </>
  )
}

interface ListParagraphProps {
  // type: 'list',
  data: {
    items: string[]
    style: string
  }
}

const ListParagraph: FC<ListParagraphProps> = ({ data }) => {
  const { items, style } = data
  return (
    <ul className={`${style === 'ordered' ? 'list-decimal' : 'list-disc'} list-inside`}>
      {items.map(item => <li>{item}</li>)}
    </ul>
  )
}

interface DelimiterParagraphProps {
  // type: 'delimiter',
  data: {}
}

const DelimiterParagraph: FC<DelimiterParagraphProps> = ({ data }) => {
  return <p className="text-center text-3xl">* * *</p>
}

const Blog = () => {
  const router = useRouter()
  console.log("id", router.query.id)

  const { loading, error, data } = useQuery(GET_BLOG, {
    variables: {
      _id: router.query.id,
    }
  })

  if (loading) return <p>Loading...</p>
  if (error) return <p>{error.message}</p>
  const content: OutputData = JSON.parse(data.blog.content)

  console.log(content)
  console.log(data.blog)
  console.log(data.blog.comments)
  
  return (
    <>
    <Layout>
      <Header />
    </Layout>
    <div className="max-w-screen-lg h-96 m-auto mb-8">
      <img className="mb-8 w-full h-96 object-cover m-auto" src={data.blog.imageHeaderUrl || '/story-image1.png'}/>
    </div>
    <Layout>
      <div className="md:flex">
        <Writer 
          __typename={data.blog.__typename}
          _id={data.blog._id}
          author={data.blog.writer.name} 
          views={data.blog.views} 
          likes={data.blog.likes}
          liked={data.blog.liked}
          saved={data.blog.saved}
          imageUrl={data.blog.writer.imageUrl}
          />
        <LayoutBlog>
          {content.blocks.map(block => {
            switch(block.type) {
              case 'header': 
                return <BlogHeader {...block}/>
              case 'paragraph': 
                return <BlogParagraph {...block} />
              case 'image':
                return <ImageParagraph {...block} />
              case 'list':
                return <ListParagraph {...block} />
              case 'delimiter':
                return <DelimiterParagraph {...block} />
            }
          })}
          <div>
            <HeaderComment>
              {data.blog.comments.map(comment => {
                return <Comment 
                  content={comment.content} 
                  name={comment.from.user.name} 
                  date={comment._createdAt} 
                  imageUrl={comment.from.user.imageUrl}/>
              })}
            </HeaderComment>
            <CreateComment blogId={data.blog._id}/>
          </div>
        </LayoutBlog>
      </div>
    </Layout>
    <Footer />
    </>
  )  
}

export default Blog
