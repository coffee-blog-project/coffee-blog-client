import Select from '../../../components/category/SelectCategory'
import Layout from '../../../components/Layout'
import Header from '../../../components/Header'

const SelectCategory = () => {
  return (
    <Layout>
      <Header />
        <Select />
    </Layout>
  )
}

export default SelectCategory