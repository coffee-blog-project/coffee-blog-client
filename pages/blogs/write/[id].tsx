import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import { gql, useMutation, useQuery } from '@apollo/client'
import { OutputData } from '@editorjs/editorjs'
import Layout from '../../../components/Layout'
import Header, { SaveDraftButton, PostButton } from '../../../components/Header'
const Editor = dynamic(() => import('../../../components/Editor'), { ssr: false })
import ImportImage from '../../../components/Importimage'
import { resizeImage } from '../../../libs/resizer'
import Axios from 'axios'

const GET_BLOG = gql`
  query GetBlog($_id: String!) {
    blog(
      _id: $_id
    ) {
      _id
      _createdAt
      _updatedAt
      writerId
      title
      imageHeaderUrl
      contentFormat
      content
    }
  }
`

const UPDATE_BLOG = gql`
  mutation UpdateBlog($blog: UpdateBlogArgs!, $_id: String!) {
    updateBlog (
      blog: $blog
      _id: $_id
    ) {
      _id
    }
  }
`

const WriteStory = () => {
  const router = useRouter()
  const _id = router.query.id

  const [blogData, setBlogData] = useState<OutputData>(null)
  const [isEdit, setIsEdit] = useState<boolean>(false) 

  const [updateBlog, updateBlogState] = useMutation(UPDATE_BLOG)

  const [imageHeader, setImageHeader] = useState<File>(null)

  const getBlogState = useQuery(GET_BLOG, {
    variables: { _id }
  })

  useEffect(() => {
    if (getBlogState.data) {
      setBlogData(JSON.parse(getBlogState.data.blog.content))
    }
  }, [getBlogState.data])

  if (getBlogState.loading || updateBlogState.loading || !blogData) 
    return <p>Loading...</p>
  if (getBlogState.error) return <p>{getBlogState.error.message}</p>

  const onButtonClick = async (published: boolean) => {

    let imageHeaderUrl: string = getBlogState.data.blog.imageHeaderUrl
    if (imageHeader) {
      const body = new FormData()
      body.append('file', await resizeImage(imageHeader, 1024, 1024))
      const { data } = await Axios.post('https://coffee-blog.markkiti.me/images', body)
      imageHeaderUrl = data.url
    }

    await updateBlog({
      variables: { _id, blog: { 
        imageHeaderUrl,
        title: blogData.blocks[0].data.text,
        content: JSON.stringify(blogData),
        published,
      }}
    })
    setIsEdit(false)
    console.log('update blog complete')
    router.push('/') 
  }

  console.log(getBlogState.data.blog.imageHeaderUrl)

  return (
    <Layout>
      <Header>
        <SaveDraftButton disabled={!isEdit} onClick={() => onButtonClick(false)} />
        <PostButton onClick={() => onButtonClick(true)} />
      </Header>
      <ImportImage 
        imageUri={getBlogState.data.blog.imageHeaderUrl}
        onChange={(e) => {
          setImageHeader(e.target.files[0])
        }}/>
      <Editor 
        data={blogData} 
        onChange={(data) => {
          setBlogData(data)
          setIsEdit(true)
        }}/>
    </Layout>
  )
}

export default WriteStory