import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import Layout from '../../../components/Layout'
import Header, { SaveDraftButton, PostButton } from '../../../components/Header'
import { gql, useMutation } from '@apollo/client'
import { useState, useEffect } from 'react'
import { OutputData } from '@editorjs/editorjs'
const Editor = dynamic(() => import('../../../components/Editor'), { ssr: false })
import ImportImage from '../../../components/Importimage'
import { resizeImage } from '../../../libs/resizer'
import Axios from 'axios'

const CREATE_BLOG = gql`
  mutation CreateBlog($blog: CreateBlogArgs!) {
	  createBlog(blog: $blog) {
      _id
    }
  }
`

const WriteStory = () => {
  const router = useRouter()
  const [blogData, setBlogData] = useState<OutputData>(null)
  const [createBlog] = useMutation(CREATE_BLOG)
  const [imageHeader, setImageHeader] = useState<File>(null)

  const category = router.query.category as string
  useEffect(() => {
    console.log(router)
    console.log(category)
    if (!category) {
      router.push('/blogs/write/select')
    }
  }, [router.query]);

  const onButtonClick = async (published: boolean) => {

    let imageHeaderUrl: string = null
    if (imageHeader) {
      const body = new FormData()
      body.append('file', await resizeImage(imageHeader, 1024, 1024))
      const { data } = await Axios.post('https://coffee-blog.markkiti.me/images', body)
      imageHeaderUrl = data.url
    }

    const { data } = await createBlog({ 
      variables: {
        blog: {
          /**
           * TODO: Sync writerId
           */
          imageHeaderUrl,
          title: blogData.blocks[0].data.text,
          contentFormat: "editorjs",
          content: JSON.stringify(blogData),
          category,
          published,
        }
      }
    })

    router.replace(`/blogs${published ? '' : '/write'}/${data.createBlog._id}`)
  }

  return (
    <Layout>
      <Header>
        <SaveDraftButton
          disabled={!blogData}
          onClick={async () => onButtonClick(false)}/>
        <PostButton 
          disabled={!blogData}
          onClick={async () => onButtonClick(true)}/>
      </Header>
      <ImportImage onChange={(e) => {
        setImageHeader(e.target.files[0])
      }}/>
      <div className="w-min px-4 py-1 bg-light-brown rounded-full my-4">
        <p className="text-white">
          {category}
        </p>
      </div>
      <Editor onChange={data => setBlogData(data)}/>
    </Layout>
  )
}

export default WriteStory