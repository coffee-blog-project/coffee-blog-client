import Layout from '../../components/Layout'
import Header from '../../components/Header'
import User from '../../components/profile/User'
import Posts from '../../components/profile/Posts'
import Menu from '../../components/profile/Menu'
import { gql, useQuery } from '@apollo/client'

const GET_MYREVIEW = gql`
  query {
    me {
      reviews {
        _id
        published
        productName
        _createdAt
        _updatedAt
        productImgUrl
        writer{
          name
        }
      }
    }
  }
`

const Profile = () => {
  const {error ,loading ,data} = useQuery(GET_MYREVIEW)

  if (loading) return <p>Loading...</p>
  if (error) return <p>{error.message}</p>

  return (
    <Layout>
      <Header />
      <div className="max-w-7xl mx-16 hidden md:block">
        <Menu />
        <div className="flex">
          <Posts data={data.me.reviews}/>
          <User imageUrl="/story-image1.png" name="Sasirat"/>
        </div>
      </div>
      <div className="max-w-7xl mx-16 mx-auto md:hidden">
        <User imageUrl="/story-image1.png" name="Sasirat"/>
        <Menu />
        <Posts data={data.me.reviews}/>
      </div>
    </Layout>
  )
}

export default Profile