import Layout from '../../components/Layout'
import Header from '../../components/Header'
import User from '../../components/profile/User'
import Post from '../../components/profile/Posts'
import Menu from '../../components/profile/Menu'
import { gql, useQuery } from '@apollo/client'
import Footer from '../../components/Footer'

const GET_MYSAVE = gql`
  query {
    me {
      save {
        to{
          _id
          type
        } 
        source {
          ...on Blog {
            _id
            title
            writer {
              name
            }
            _createdAt
            imageHeaderUrl
          }
          ...on Review {
            _id
            productName
            writer {
              name
            }
            _createdAt
            productImgUrl
          }
        }
      }
    }
  }
`

const Profile = () => {
  const {error, loading, data} = useQuery(GET_MYSAVE)

  if (loading) return <p>Loading...</p>
  if (error) return <p>{error.message}</p>

  const save = data.me.save.map(item => item.source)

  return (
    <>
    <Layout>
      <Header />
      <div className="max-w-7xl mx-16 hidden md:block">
        <Menu />
        <div className="md:flex">
          <Post data={save}/>
          <User imageUrl="/story-image1.png" name="Sasirat"/>
        </div>
      </div>
      <div className="max-w-7xl mx-16 mx-auto md:hidden">
        <User imageUrl="/story-image1.png" name="Sasirat"/>
        <Menu />
        <Post data={save}/>
      </div>
    </Layout>
    <Footer />
    </>
  )
}

export default Profile