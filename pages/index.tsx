import React from 'react'
import Header, { SearchButton } from '../components/Header'
import Layout from '../components/Layout'
import ShowRank from '../components/Rank'
import Catagories from '../components/Catagories'
import Stories from '../components/Stories'
import Reviews from '../components/Review'
import Footer from '../components/Footer'


const Home = () => {
  return (
    <>
    <Layout>
      <Header>
        <SearchButton />
      </Header>
      <div className="sm:flex block mb-8 mt-8">
        <div className="sm:w-64 sm:flex-none sm:mr-8">
          <Catagories />
          <ShowRank />
        </div>
        <Stories />
      </div>
      <Reviews />
    </Layout>
    <Footer />
    </>
  )
}
export default Home