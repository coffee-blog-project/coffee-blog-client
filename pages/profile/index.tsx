import { FC } from 'react'
import Layout from '../../components/Layout'
import Header from '../../components/Header'
import NextLink from 'next/link'
import { useRouter } from 'next/router'

interface LinkProps {
  href: string
  children: string
}

const Link: FC<LinkProps> = ({ href, children }) => {
  const router = useRouter()
  const active = href === router.pathname
  return (
    <NextLink href={href}>
      <p className={`cursor-pointer ${active && 'text-black'}`} >{children}</p>
    </NextLink>
  )
}

export const User = () => {
  return (
    <div className="mb-8">
      <div className="flex">
        <img className="rounded rounded-full w-24 h-24" src="/story-image1.png"/>
        <p className="mt-10 ml-10">Sasirat</p>
      </div>
    </div>
  )
}

const Profile = () => {
  return(
    <Layout>
      <Header />
      <div className="mt-16 container lg:pr-20 mx-auto lg:pl-12 md:pl-10">
        <User />
        <hr />
        <div className="flex my-8 text-gray-400 active:text-black">
        <div className="mr-8">
          <Link href="/profile">Story</Link>
          <hr />
        </div>
        <div className="mr-8">
          <Link href="/profile">Review</Link>
          <hr />
        </div>
      </div>
    </div>
    </Layout>
  )
}

export default Profile