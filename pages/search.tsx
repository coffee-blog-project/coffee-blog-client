import { useState, FC } from "react";
import Header from "../components/Header";
import Layout from "../components/Layout";
import { useLazyQuery, gql } from "@apollo/client";
import { DateTime } from "luxon";
import Footer from "../components/Footer";
import NextLink from 'next/link'

export interface SearchProps {
  _id: string;
  title: string;
  image: string;
  writer: {
    name: string;
  };
  _createdAt: string;
  __typename: string;
}

const SearchResult: FC<SearchProps> = ({
  _id,
  title,
  image,
  writer,
  _createdAt,
  __typename,
}) => {
  return (
  <NextLink href={`/${__typename.toLowerCase()}s/${_id}`}>
      <div className="flex mb-6 cursor-pointer">
        <div className="w-36 h-24 flex-none">
          <img className="w-full h-full object-cover" src={image || '/story-image1.png'} />
        </div>
        <div className="ml-4">
          <p>{title}</p>
          <p className="text-gray-400 text-sm">
            <span className="mr-1">{writer.name},</span>
            <span>
              {DateTime.fromISO(_createdAt).toLocaleString(DateTime.DATE_FULL)}
            </span>
          </p>
        </div>
      </div>
    </NextLink>
  );
};

const GET_SEARCH = gql`
  query getSearch($query: String!) {
    search(query: $query) {
      ... on Blog {
        _id
        title
        imageHeaderUrl
        writer {
          name
        }
        _createdAt
      }

      ... on Review {
        _id
        productName
        productImgUrl
        writer {
          name
        }
        _createdAt
      }
    }
  }
`;

const Search = () => {
  const [query, setQuery] = useState<string>("");
  const [getSearch, { loading, error, data }] = useLazyQuery(GET_SEARCH);
  const blogs = data
    ? data.search.filter((item) => item.__typename === "Blog")
    : [];
  const reviews = data
    ? data.search.filter((item) => item.__typename === "Review")
    : [];

  return (
    <>
      <Layout>
        <Header />
        <form
          onSubmit={(e) => {
            e.preventDefault();
            console.log(query);
            getSearch({
              variables: {
                query,
              },
            });
          }}
          className="m-auto mt-16"
        >
          <input
            className="border-b-0.5 border-black p-4 w-2/3 outline-none mb-8 text-xl"
            placeholder="Search..."
            value={query}
            onChange={(e) => setQuery(e.target.value)}
          />
        </form>
        <div>
          {loading && <p>Loading...</p>}
          {error && <p>{error.message}</p>}
          {data && blogs.length === 0 && reviews.length === 0 && (
            <div className="mx-auto mt-12 mb-24">
              <p className="text-center font-mono">Not found</p>
              <img className="w-80 mx-auto" src="/notfound.png"/>
            </div>
          )}
          {blogs.length !== 0 && (
            <div>
              <p className="text-2xl font-serif mb-4">Stories</p>
              {blogs.map((item) => (
                <SearchResult {...item} image={item.imageHeaderUrl} />
              ))}
            </div>
          )}

          {reviews.length !== 0 && (
            <div>
              <p className="text-2xl font-serif mb-4">Reviews</p>
              {reviews.map((item) => (
                <SearchResult {...item} title={item.productName} image={item.productImgUrl}/>
              ))}
            </div>
          )}
        </div>
      </Layout>
      <Footer />
    </>
  );
};

export default Search;
