import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { gql, useMutation } from '@apollo/client'
import getConfig from 'next/config'

const RESLOVE_FACEBOOK_OAUTH = gql`
  mutation ResolveFacebookOAuth(
    $code: String!
    $redirectUri: String!
  ) {
    resolveFacebookOAuth(code: $code, redirectUri: $redirectUri)
  }
`

const Callback = () => {
  const router = useRouter() 
  const config = getConfig()
  const [resolve] = useMutation(RESLOVE_FACEBOOK_OAUTH)

  useEffect(() => {
    const fetch = async () => {
      if (router.query.code) {
        const { code } = router.query
        const { data } = await resolve({
          variables: {
            code,
            redirectUri: new URL('/facebook/callback', config.publicRuntimeConfig.APP_URL).href,
          }
        })

        localStorage.setItem('token', data.resolveFacebookOAuth)
        router.push('/')
      }
    }
    fetch()
  }, [router.query])


  useEffect(() => {
    console.log(router.query)
  }, [router.query])

  return null
}

export default Callback