import { FC, InputHTMLAttributes, useEffect, useState, FormEventHandler } from 'react'
import Layout from '../components/Layout'
import Header from '../components/Header'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faUserCircle} from '@fortawesome/free-solid-svg-icons'
import {faCamera} from '@fortawesome/free-solid-svg-icons'
import {gql, useQuery, useMutation} from '@apollo/client'
import Axios from 'axios'
import FormData from 'form-data'
import { useRouter } from 'next/router'
import { resizeImage } from '../libs/resizer'

export interface SettingsPorps {
  imageUrl?: string
  name?: string
}

const GET_ME = gql`
  query {
    me {
      _id
      name
      email
      imageUrl
    }
  }
`

const UPDATE_ME = gql`
  mutation UpdateMe($user: UpdateUserArgs!) {
    updateUser(user: $user) {
      _id
      name
      email
    }
  }
`

const CameraButton: FC<InputHTMLAttributes<HTMLInputElement>> = (props) => {
  const [hover, setHover] = useState(false)
  return (
    <div 
      className="absolute m-auto w-24 h-24 text-center flex"
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      >
      <div className="m-auto md:block hidden">
        <input 
          {...props}
          type="file" id="upload" hidden 
          />
        {hover && <label htmlFor="upload" >
          <FontAwesomeIcon className="text-2xl " icon={faCamera}/>
          <p>edit</p>
        </label>}
      </div>
      <div className="m-auto md:hidden block">
        <input 
          {...props}
          type="file" id="upload" hidden  
          />
        <label htmlFor="upload" >
          <FontAwesomeIcon className="text-2xl block" icon={faCamera}/>
          <p>edit</p>
        </label>
      </div>
    </div>
  )
}

const Settings: FC<SettingsPorps> = () => {
  const router = useRouter()
  const { data, loading, error } = useQuery(GET_ME, {
    fetchPolicy: 'network-only',
  })

  const [updateMe] = useMutation(UPDATE_ME)

  const [updateInfo, setUpdateInfo] = useState<SettingsPorps>({})
  const [uploadFile, setUploadFile] = useState<File>(null)

  useEffect(() => {
    setUpdateInfo(data?.me || {}) 
  }, [data])

  const onUpdateMe: FormEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault()
    let userImage: string = updateInfo.imageUrl
    if (uploadFile) {
      const body = new FormData()
      body.append('file', await resizeImage(uploadFile))
      const { data } = await Axios.post('https://coffee-blog.markkiti.me/images', body)
      userImage = data.url
    }

    await updateMe({
      variables: {
        user: {
          name: updateInfo.name,
          imageUrl: userImage,
        }
      }
    })

    router.push('/my/save')
  }
  
  if (loading) return <p>Loading...</p>
  if (error) return <p>{error.message}</p>

  const { imageUrl, name, email } = data.me

  return (
    <Layout>
      <Header />
      <div className="container lg:pr-20 mx-auto lg:pl-12 md:pl-10 mt-16 mx-40" >
        <p className="font-serif text-3xl mb-4">Settings</p>
        <hr  className="mb-8"/>
        <div className="md:flex">
          <CameraButton onChange={(e) => setUploadFile(e.target.files[0])} />
        <div className="lg:w-30 lg:h-30 md:w-24 md:h-24 mb-4 w-24 h-24">
          {uploadFile || imageUrl ? (
            <img
              className="object-cover w-full h-full rounded-full" 
              src={uploadFile ? URL.createObjectURL(uploadFile) : imageUrl} />
          ) : (
            <FontAwesomeIcon className="mb-4 text-8xl text-gray-300 w-full" icon={faUserCircle}/>
          )}
        </div>
        <form className="md:ml-16 m-4" onSubmit={onUpdateMe}>
          <p className="mb-2">name</p>
          <input 
            className="border border-gray-300 outline-none rounded-md py-1 px-4 md:w-96 w-full mb-4" 
            type="text" 
            value={updateInfo.name}
            onChange={(e) => setUpdateInfo({
              ...updateInfo,
              name: e.target.value,
            })}
            placeholder={name || "username"} />
          <p className="mb-2">email</p>
          <input 
            className="border border-gray-300 outline-none rounded-md py-1 px-4 md:w-96 w-full" 
            type="text" 
            value={email} 
            placeholder={email} 
            disabled />
          <div className="flex justify-end mt-8">
            <button className="border border-1 border-black py-1 px-3 rounded-full">save</button>
          </div>
        </form>
        </div>
      </div>
    </Layout>
  )
}

export default Settings