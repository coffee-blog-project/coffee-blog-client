import dynamic from 'next/dynamic'
const Editor = dynamic(() => import('../components/Editor'), { ssr: false })

const EditorPage = () => {
  return (
    <Editor /> 
  )
}

export default EditorPage