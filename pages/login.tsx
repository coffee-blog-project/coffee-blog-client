import LoginForm from '../components/Login'

const Login = () => {
  return (
    <div className="h-screen">
      <div className="container flex m-auto  h-full justify-center items-center">
        <div className="h-screen md:h-full flex" >
            <img 
              className="object-center object-cover w-6/12 hidden xl:block"
              src="img-register@2x.png" 
            />
          <div className="px-6 xl:px-20">
            <LoginForm />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Login