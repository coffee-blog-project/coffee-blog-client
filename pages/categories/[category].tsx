import { useRouter } from 'next/router'
import CategoryName from '../../components/category/Name'
import React from 'react'
import Header, { SearchButton } from '../../components/Header'
import Layout from '../../components/Layout'
import ShowRank from '../../components/Rank'
import Catagories from '../../components/Catagories'
import Footer from '../../components/Footer'
import { Story } from '../../components/Stories'
import { gql, useQuery } from '@apollo/client'
import { getEnabledCategories } from 'trace_events'

const GET_BLOG = gql`
  query getBlogs($category: CategoryType) {
    blogs(category: $category) {
      _id
      _createdAt
      title
      writer {
        _id
        name
      }
    }
  }
`
const Process = () => {
  const router = useRouter()
  const category = router.query.category as string
  
  if (!category) {
    return null
  }

  const getBlog = useQuery(GET_BLOG, {
    variables: {
      category,
    }
  })

  if (getBlog.loading) 
    return <p>loading...</p>

  if (getBlog.error)
    return <p>{getBlog.error.message}</p>

  return (
    <>
    <Layout>
      <Header>
        <SearchButton />
      </Header>
      <div className="sm:flex block mb-8">
        <div className="sm:w-64 sm:flex-none sm:mr-8">
          <Catagories />
          <ShowRank />
        </div>
        <div className="w-full">
          <CategoryName name={category.toUpperCase()}/>
          <div className="lg:grid lg:grid-cols-2 lg:gap-x-6 lg:gap-y-4 block">
            {getBlog.data.blogs.map(story => <Story key={story._id} {...story} />)}
          </div>
        </div>
      </div>
    </Layout>
    <Footer />
    </>
  )
}
export default Process