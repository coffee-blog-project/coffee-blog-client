import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { gql, useMutation } from '@apollo/client'
import getConfig from 'next/config'

const RESLOVE_GOOGLE_OAUTH = gql`
  mutation ResolveGoogleOAuth(
    $code: String!
    $redirectUri: String!
  ) {
    resolveGoogleOAuth(code: $code, redirectUri: $redirectUri)
  }
`

const Callback = () => {
  const router = useRouter() 
  const config = getConfig()
  const [resolve] = useMutation(RESLOVE_GOOGLE_OAUTH)

  useEffect(() => {
    const fetch = async () => {
      if (router.query.code) {
        const { code } = router.query
        const { data } = await resolve({
          variables: {
            code,
            redirectUri: new URL('/google/callback', config.publicRuntimeConfig.APP_URL).href,
          }
        })

        localStorage.setItem('token', data.resolveGoogleOAuth)
        router.push('/')
      }
    }
    fetch()
  }, [router.query])

  return null
}

export default Callback