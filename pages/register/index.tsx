import React from 'react'
import Register from '../../components/Register'

const RegisterPage = () => {
  return (
    <div className="h-screen">
      <div className="container flex m-auto  h-full justify-center items-center">
        <div className="h-screen md:h-full flex" >
            <img 
              className="object-center object-cover w-6/12 hidden xl:block"
              src="img-register@2x.png" 
            />
          <div className="px-10 xl:px-20">
            <Register />
          </div>
        </div>
      </div>
    </div>
  )
}

export default RegisterPage