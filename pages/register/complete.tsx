import SubmitButton from '../../components/SubmitButton'
import NextLink from 'next/link'

const RegisterComplete = () => {
  return (
    <div className="h-screen flex">
      <div className="m-auto w-96">
        <img className="w-180 mx-auto mb-8" src="/coffee_break.png" />
        <NextLink href="/login">
          <SubmitButton>Please Log in</SubmitButton>
        </NextLink> 
      </div>
    </div>
  )
}

export default RegisterComplete