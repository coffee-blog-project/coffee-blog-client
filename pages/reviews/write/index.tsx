import { useRouter } from 'next/router'
import Rate from '../../../components/reviews/Rate'
import Layout from '../../../components/Layout'
import Header, { SaveDraftButton, PostButton } from '../../../components/Header'
import FlavorWheel, { FlavorWheelValue } from '../../../components/reviews/FlavorWheel'
import ReviewLayout from '../../../components/reviews/ReviewLayout'
import ReviewForm from '../../../components/reviews/ReviewForm'
import { gql, useMutation } from '@apollo/client'
import { useState, useEffect } from 'react'
import ImportImage from '../../../components/Importimage'
import { resizeImage } from '../../../libs/resizer'
import Axios from 'axios'

const CREATE_REVIEW = gql`
  mutation CreateReview($review: CreateReviewArgs!) {
	  createReview(review: $review) {
      _id
    }
  }
`

interface ReviewData {
  productName?: string
  description?: string
  rate?: {
    sweet?: number
    bitter?: number
    source?: number
    roasted?: number
    fragrance?: number
  }
  flavors?: FlavorWheelValue
}

const CreateReview = () => {
  const router = useRouter()
  const [reviewData, setReviewData] = useState<ReviewData>({})
  const [productImage, setProductImage] = useState<File>(null)
  const [createReview] = useMutation(CREATE_REVIEW)
  useEffect(() =>{
    console.log(reviewData)
  }, [reviewData])

  const onButtonClick = async (published: boolean) => {
    let productImgUrl: string = null
    if (productImage) {
      const body = new FormData()
      body.append('file', await resizeImage(productImage, 1024, 1024))
      const { data } = await Axios.post('https://coffee-blog.markkiti.me/images', body)
      productImgUrl = data.url
    }

    const { data } = await createReview({
      variables: {
        review: {
          productName: reviewData.productName,
          productImgUrl,
          content: JSON.stringify(reviewData),
          published,
        }
      }
    })
    router.push('/')
  }

  return (
    <Layout>
       <Header>
        <SaveDraftButton onClick={() => onButtonClick(false)}/>
        <PostButton onClick={async () => onButtonClick(true)} />
      </Header>
      <ReviewLayout>
        <ImportImage onChange={(e) => setProductImage(e.target.files[0])} />
        <ReviewForm onChange={(data) => setReviewData({ ...reviewData, ...data })}/>
        <div className="grid md:grid-cols-2 mx-auto">
          <Rate 
            onChange={(data) => setReviewData({
              ...reviewData, 
              rate: { 
                ...reviewData.rate,
                sweet: data,
              }
            })} 
            title="sweet"/>
          <Rate 
            onChange={(data) => setReviewData({
              ...reviewData, 
              rate: { 
                ...reviewData.rate,
                bitter: data,
              }
            })} 
            title="bitter"/>
          <Rate 
            onChange={(data) => setReviewData({
              ...reviewData, 
              rate: { 
                ...reviewData.rate,
                source: data,
              }
            })} 
            title="source"/>
          <Rate 
            onChange={(data) => setReviewData({
              ...reviewData, 
              rate: { 
                ...reviewData.rate,
                roasted: data,
              }
            })} 
            title="roasted"/>
          <Rate 
            onChange={(data) => setReviewData({
              ...reviewData, 
              rate: { 
                ...reviewData.rate,
                fragrance: data,
              }
            })} 
            title="fragrance"/>
        </div>
        <FlavorWheel onChange={value => setReviewData({
          ...reviewData,
          flavors: value,
        })} />
      </ReviewLayout>
    </Layout>
  )
}

export default CreateReview