import { useRouter } from 'next/router'
import Rate from '../../../components/reviews/Rate'
import Layout from '../../../components/Layout'
import Header, { SaveDraftButton, PostButton } from '../../../components/Header'
import FlavorWheel, { FlavorWheelValue } from '../../../components/reviews/FlavorWheel'
import ReviewLayout from '../../../components/reviews/ReviewLayout'
import ReviewForm from '../../../components/reviews/ReviewForm'
import { gql, useMutation, useQuery } from '@apollo/client'
import { useState, useEffect } from 'react'
import ImportImage from '../../../components/Importimage'
import { resizeImage } from '../../../libs/resizer'
import Axios from 'axios'

const GET_REVIEW = gql`
  query GetReview($_id: String!) {
    review(
      _id: $_id
    ) {
      _id
      _createdAt
      _updatedAt
      writerId
      productName
      productImgUrl
      content
    }
  }
`

const UPDATE_REVIEW = gql`
  mutation UpdateReview($review: UpdateReviewArgs!, $_id: String!) {
    updateReview (
      review: $review
      _id: $_id
    ) {
      _id
    }
  }
`

interface ReviewData {
  productName?: string
  description?: string
  rate?: {
    sweet?: number
    bitter?: number
    source?: number
    roasted?: number
    fragrance?: number
  }
  flavors?: FlavorWheelValue
}

const CreateReview = () => {
  const router = useRouter()
  const _id = router.query.id

  const [reviewData, setReviewData] = useState<ReviewData>({})
  const [productImage, setProductImage] = useState<File>(null)
  const [isEdit, setIsEdit] = useState<boolean>(true) 

  const getReview = useQuery(GET_REVIEW, {
    variables: { _id }
  })

  const [updateReview] = useMutation(UPDATE_REVIEW)

  useEffect(() => {
    console.log(getReview.data)
    if (getReview.data) {
      const { productName, content } = getReview.data.review
      setReviewData({
        productName,
        ...JSON.parse(content),
      })
    }
  }, [getReview.data])

  useEffect(() => {
    setIsEdit(true)
  }, [reviewData])

  if (getReview.loading) return <p>Loading...</p>
  if (getReview.error) return <p>{getReview.error.message}</p>

  const onButtonClick = async (published: boolean) => {
    let productImgUrl: string = getReview.data.review.productImgUrl
    if (productImage) {
      const body = new FormData()
      body.append('file', await resizeImage(productImage, 1024, 1024))
      const { data } = await Axios.post('https://coffee-blog.markkiti.me/images', body)
      productImgUrl = data.url
    }

    const { data } = await updateReview({
      variables: { _id, review: { 
        productName: reviewData.productName,
        content: JSON.stringify(reviewData),
        productImgUrl,
        published,
      }}
    })
    setIsEdit(false)
    console.log('update review complete')
    router.push('/')
  }
    

  return (
    <Layout>
       <Header>
        <SaveDraftButton disabled={!isEdit} onClick={() => onButtonClick(false)} />
        <PostButton  onClick={() => onButtonClick(true)}/>
      </Header>
      <ReviewLayout>
        <ImportImage imageUri={getReview.data.review.productImgUrl} onChange={(e) => setProductImage(e.target.files[0])} />
        <ReviewForm value={reviewData} onChange={(data) => setReviewData({ ...reviewData, ...data })}/>
        <div className="grid md:grid-cols-2 mx-auto">
          <Rate 
            onChange={(data) => setReviewData({
              ...reviewData, 
              rate: { 
                ...reviewData.rate,
                sweet: data,
              }
            })}
            value={reviewData.rate?.sweet}
            title="sweet"/>
          <Rate 
            onChange={(data) => setReviewData({
              ...reviewData, 
              rate: { 
                ...reviewData.rate,
                bitter: data,
              }
            })} 
            value={reviewData.rate?.bitter}
            title="bitter"/>
          <Rate 
            onChange={(data) => setReviewData({
              ...reviewData, 
              rate: { 
                ...reviewData.rate,
                source: data,
              }
            })} 
            value={reviewData.rate?.source}
            title="source"/>
          <Rate 
            onChange={(data) => setReviewData({
              ...reviewData, 
              rate: { 
                ...reviewData.rate,
                roasted: data,
              }
            })} 
            value={reviewData.rate?.roasted}
            title="roasted"/>
          <Rate 
            onChange={(data) => setReviewData({
              ...reviewData, 
              rate: { 
                ...reviewData.rate,
                fragrance: data,
              }
            })} 
            value={reviewData.rate?.fragrance}
            title="fragrance"/>
        </div>
        <FlavorWheel 
          value={reviewData.flavors} 
          onChange={value => setReviewData({
            ...reviewData,
            flavors: value,
          })} />
      </ReviewLayout>
    </Layout>
  )
}

export default CreateReview