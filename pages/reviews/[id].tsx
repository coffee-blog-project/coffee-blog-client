import Layout from '../../components/Layout'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Colors from '../../components/reviews/Color' 
import { gql, useQuery } from '@apollo/client'
import { useRouter } from 'next/router'
import Writer from '../../components/blogs/Writer'
import Graph from '../../components/reviews/Chart'

const GET_REVIEW = gql`
  query GetReview($_id: String!) {
    review(
      _id: $_id
    ) {
      _id
    	_createdAt
    	productName
      productImgUrl
    	content
    	writerId
      likes
    	liked
      saved
      views
      writer {
        _id
        name
        _createdAt
        imageUrl

      }
    }
  }
`

const Review = () => {
  const router = useRouter()
  console.log("id", router.query.id)

  const { loading, error, data } = useQuery(GET_REVIEW, {
    variables: {
      _id: router.query.id,
    }
  })

  if (loading) return <p>loading...</p>
  if (error) return <p>{error.message}</p>

  const content = JSON.parse(data.review.content) 
  console.log(content)

  return (
    <>
      <Layout>
        <Header />
        <div className="max-w-xl mx-auto">
          <div className="aspect-w-3 aspect-h-3 my-6">
            <img className="mx-auto w-full h-full object-cover" src={data.review.productImgUrl || '/story-image1.png'} />
          </div>
          <div className="md:flex w-full">
            <div className="lg:-ml-28 md:-ml-24 md:mr-6">
              <Writer 
                __typename={data.review.__typename}
                _id={data.review._id}
                author={data.review.writer.name}
                views={data.review.views}
                likes={data.review.likes}
                liked={data.review.liked}
                saved={data.review.saved}
                imageUrl={data.review.writer.imageUrl}
              />
            </div>
            <div className="w-full">
              <Colors flavors={content.flavors} />
              <p className="font-header text-3xl mb-4 mt-4">{data.review.productName}</p>
              <p className="mb-8">{content.description}</p>
              <div className="mb-16">
              <Graph {...content.rate}/>
              </div>
            </div>
            </div>
        </div>
      </Layout>
      <Footer />
    </>
  )
}

export default Review